#!/usr/bin/env php
<?php
const XDGOPEN = "/data/data/com.termux/files/usr/bin/xdg-open";
const TMPLATE = "tmpxdgopen";
define('TMPDIR', sys_get_temp_dir());
foreach (glob(TMPDIR . '/' . TMPLATE. '.*') as $file) {
	unlink($file);
}
$stdin = false;
$args = [];
for ($i=1; $i<$argc; $i++) {
	$value = $argv[$i];
	if ($value === '-' && !$stdin) {
		$stdin = true;
	} else {
		$args[] = $value;
	}
}
$command = XDGOPEN;
if ($stdin) {
	$name = tempnam(TMPDIR, TMPLATE . '.');
	file_put_contents($name, file_get_contents('php://stdin'));
	$type = mime_content_type($name);
	$command .= ' ' . escapeshellarg($name) . ' --content-type=' . escapeshellarg($type);
}
foreach ($args as $value) {
	$command .= ' ' . escapeshellarg($value);
}
system($command, $exit_code);
return $exit_code;
