A tiny overlay shell script for termux xdg-open. It saves the command output to temp file and then passes to xdg-open.

1. Copy `xdg-open` file to `~/.local/bin` and give it execution permission.
2. Pipe anything (text, jpeg file, whatever) into `xdg-open -`.
3. File will be saved in `$TMPDIR/tmpxdgopen.*` and the previous file will be deleted.
4. This file will be passed to standard xdg-open, which will cause Termux to display app choosing dialog. This script automatically recognizes file type, so if you pipe e.g. image, you'll see a list of image viewers.

> Update: there is a more consistent PHP variant of this script, because it deals better with spaces in file name. You need to have php installed. In free time I plan to rewrite it to Python, because it's out-of-the box in Termux.

This is not a complete substitute for Unix pipes, because incomplete output cannot be passed forward.

This script contains simple garbage collection system - there is only one latest temporary file stored, so if you open one file, then another, the previous one is lost and if app hadn't cached it before, it complains about deleted file.

## License

GPLv3 as always.

    termux xdg-open pipe
    Copyright (C) 2022  Anedroid

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
